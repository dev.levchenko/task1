function EmailValidation(target: any) {
    return class extends target {
        public isValid: boolean;

        constructor(...args: any) {
            super(...args);

            const regExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
            this.isValid = regExp.test(this.email);
        }
    }
}

// @ts-ignore
@EmailValidation
class Person {

    public email: string;

    constructor(email: string) {
        this.email = email;
    }

}

const person = new Person('fff@gmail.com');
console.log(person);
