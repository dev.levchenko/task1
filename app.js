"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
function EmailValidation(target) {
    return class extends target {
        constructor(...args) {
            super(...args);
            const regExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
            this.isValid = regExp.test(this.email);
        }
    };
}
// @ts-ignore
let Person = class Person {
    constructor(email) {
        this.email = email;
    }
};
Person = __decorate([
    EmailValidation
], Person);
const person = new Person('fff@gmail.com');
console.log(person);
